package com.goddy.basic_annotation;

import com.goddy.basic_annotation.util.Base64Util;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BasicAnnotationApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void encodeTest() {
        log.info(Base64Util.encode("wcm, a beautiful girl. r\\ig/ht? "));
	}

}
