package com.goddy.basic_annotation.controller;

import com.goddy.basic_annotation.annotation.DecodeAnnotation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Goddy
 * @Date Create in 下午4:44 2018/4/17
 */
@RestController
@RequestMapping("/uid/{uid}/")
public class UserController {

    @RequestMapping("/book/{book}")
    public Object getBookInfo(@DecodeAnnotation String uid, @PathVariable String book) {
        return "uid:" + uid + "<br>book:" + book;
    }
}
