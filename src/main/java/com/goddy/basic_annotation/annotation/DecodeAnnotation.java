package com.goddy.basic_annotation.annotation;

import java.lang.annotation.*;

/**
 * @Author Goddy
 * @Date Create in 下午4:40 2018/4/17
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DecodeAnnotation {

}
