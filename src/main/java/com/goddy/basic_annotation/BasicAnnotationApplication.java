package com.goddy.basic_annotation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasicAnnotationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicAnnotationApplication.class, args);
	}
}
