package com.goddy.basic_annotation.util;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;

/**
 * @Author Goddy
 * @Date Create in 下午4:42 2018/4/17
 */
public class Base64Util {

    /**
     * 二进制数据编码为BASE64字符串
     * @param str 输入数据
     * @return
     */
    public static String encode(final String str) {
        try {
            return new String(Base64.encodeBase64(str.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 解密
     * @param str
     * @return
     */
    public static String decode(final String str) {
        try {
            return new String(Base64.decodeBase64(str.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
